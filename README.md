# Socket TCP/UDP
Repositorio que contiene la unión de dos programas: cliente y servidor.
 - Cliente: programa que permite realizar peticiones persistentes (TCP) pudiendo realizar peticiones continuadas sin perder la conexión o con el protocolo UDP realizando una única petición.
 - Servidor: programa que atiende tanto peticiones TCP como UDP  a la vez. Se encuentra escuchando en un puerto las peticiones UDP y en otro las TCP. Por cada nueva petición crea un nuevo hilo y responde con un proverbio elegido de forma aleatoria del fichero proverbios.txt.

# Integrantes
 - Moisés Carral Ortiz (Expploitt)
 - Alberto Estévez Caldas (Cloclick)
